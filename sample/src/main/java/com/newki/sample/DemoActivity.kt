package com.newki.sample

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.newki.sample.databinding.ShatterRegisterButtonBinding
import com.newki.sample.databinding.ShatterRegisterText2Binding
import com.newki.sample.databinding.ShatterRegisterTextBinding
import com.newki.shatter.Shatter
import com.newki.shatter.ShatterManager


class DemoActivity : AppCompatActivity() {

    lateinit var buttonShatter: ShatterButton
    protected var mShatterManager: ShatterManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_demo)

        //预加载Shatter
        mShatterManager = ShatterManager(this)
        buttonShatter = ShatterButton()
        mShatterManager?.preloadShatter(buttonShatter, isAsync = false)

        findViewById<Button>(R.id.btn_load_shatter).setOnClickListener {

            loadShatter()
        }

    }

    private fun loadShatter() {
        mShatterManager
            ?.addShatter(R.id.fl_content, ShatterText(), isAsync = true, index = 0)
            ?.addShatter(R.id.fl_content, buttonShatter, index = 2)
            ?.addShatter(R.id.fl_content, ShatterText2(), isAsync = false, index = 1)
            ?.sendShatterEvent("Message1", "来自Activity，加载Shatter按钮的消息")
    }


    inner class ShatterButton : Shatter() {

        override fun getLayoutResId(): Int = R.layout.shatter_register_button

        override fun onCreate(intent: Intent?) {
            super.onCreate(intent)
            Log.w("MainActivity", "ShatterButton -> onCreate")
        }

        override fun initView(view: View?, intent: Intent?) {
            super.initView(view, intent)
            Log.w("MainActivity", "ShatterButton -> initView")
            val binding = getBinding<ShatterRegisterButtonBinding>()

            binding.btnRegister1.setOnClickListener {
                Toast.makeText(applicationContext, "发送了消息", Toast.LENGTH_SHORT).show()
                sendShatterEvent("Message1", "我是来自ButtonShatter的信息")
            }
        }

        override fun initData(intent: Intent?) {
            super.initData(intent)
            Log.w("MainActivity", "ShatterButton -> initData")
        }

        override fun onResume() {
            super.onResume()
            Log.w("MainActivity", "ShatterButton -> onResume")
        }

        override fun onPause() {
            super.onPause()
            Log.w("MainActivity", "ShatterButton -> onPause")
        }

        override fun onRestart() {
            super.onRestart()
            Log.w("MainActivity", "ShatterButton -> onRestart")
        }

        override fun onStart() {
            super.onStart()
            Log.w("MainActivity", "ShatterButton -> onStart")
        }

        override fun onStop() {
            super.onStop()
            Log.w("MainActivity", "ShatterButton -> onStop")
        }

        override fun onDestroy() {
            super.onDestroy()
            Log.w("MainActivity", "ShatterButton -> onDestroy")
        }


    }

    inner class ShatterText : Shatter() {

        override fun getLayoutResId(): Int = R.layout.shatter_register_text

        override fun initView(view: View?, intent: Intent?) {
            super.initView(view, intent)
            val binding = getBinding<ShatterRegisterTextBinding>()

            binding.tvReLogin.setOnClickListener {
                Toast.makeText(applicationContext, "点击重新登录", Toast.LENGTH_SHORT).show()
            }
        }

    }

    inner class ShatterText2 : Shatter() {
        private lateinit var binding: ShatterRegisterText2Binding

        override fun getLayoutResId(): Int = R.layout.shatter_register_text2

        override fun initView(view: View?, intent: Intent?) {
            super.initView(view, intent)
            binding = getBinding<ShatterRegisterText2Binding>()
        }

        override fun onShatterEvent(key: String, data: Any?) {
            super.onShatterEvent(key, data)
            when (key) {
                "Message1" -> {
                    binding.tvMessage.text = data?.toString()
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        mShatterManager?.onNewIntent(intent)
    }

    override fun onRestart() {
        super.onRestart()
        mShatterManager?.onRestart()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mShatterManager?.onActivityResult(requestCode, resultCode, data)
    }

    override fun onBackPressed() {
        if (mShatterManager != null) {
            if (mShatterManager!!.enableOnBackPressed()) {
                super.onBackPressed()
            }
        } else {
            super.onBackPressed()
        }
    }
}
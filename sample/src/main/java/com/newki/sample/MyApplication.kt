package com.newki.sample

import android.app.Application
import com.newki.shatter.ShatterManager

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        ShatterManager.init(this)
    }
}
package com.view.shatter


import android.content.Intent
import android.os.Bundle

interface ShatterLifecycleListener {

    // 通用的生命周期
    fun onStart()

    fun onResume()

    fun onPause()

    fun onStop()

    fun onDestroy()

    fun onRestart()

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)

    //Activity 的特有生命周期
    fun onNewIntent(intent: Intent?)

    fun onSaveInstanceState(outState: Bundle?)

    fun onRestoreInstanceState(savedInstanceState: Bundle?)

    fun enableOnBackPressed(): Boolean

    //Fragment 的特有回调
    fun onHiddenChanged(isHidden: Boolean)

}